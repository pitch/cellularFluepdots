# flipdot putter
putting cellular automatons on fluepdot displays in your local network

## TODO
- using arguments to change ruleset, interval etc
- stacking of fluepdots
- detect available fluepdots
    arp-scan?
- only flip changed dots instead of resending the whole framebuffer?
