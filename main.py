#! /usr/bin/env python3

import pyceau
from time import sleep
import fluepdotAPI as fd
from fluepdotAPI import Mode

# fluepdot panel properties
heightPanel = 16
widthPanel = 115
tiles = " X"

rule = '23/3' # defaulting to GOL ruleset
ip = "http://fluepdot.fd" # arp scan??
interval = 5

def framebuffer2fluepdot(ip, framebuffer):
    url = ip,
    # post(url, data=framebuffer)
    print("sending ", framebuffer, " to ", ip)

def board2framebuffer(board): # heavily inspired by flxai
    ret = ''
    for row in board.board:
        for pix in row:
            ret += tiles[pix]
        ret += '\n'
    return ret

def main():
    # defining the board and initialize it
    board = pyceau.Board (widthPanel, heightPanel, rule, tiles, flicker_mode=1)
    fd.set_url(ip)
    fd.set_mode(Mode.DIFFERENTIAL)

    while True:
        board.step()
        framebuffer = board2framebuffer(board)

        fd.post_frame_raw(framebuffer)
        sleep(interval)

if __name__ == "__main__":
    main()
