import pyceau
from main import board2framebuffer

# fluepdot panel properties
heightPanel = 16
widthPanel = 115
tiles = ' X'
rule = '0/0'  # defaulting to GOL ruleset


# to run tests:
# poetry run pytest test_main.py
# or simply, if you are already in virtual environment:
# pytest test_main.py

# test classes have to start with "Test"
class TestClass:
    # test methods have to start with test_
    def test_one(self):
        x = "this"
        assert "h" in x

    def test_two(self):
        assert 1 == 1


class TestFrameBuffer:
    def test_board2framebuffer(self):
        board = pyceau.Board(widthPanel, heightPanel, rule, tiles, flicker_mode=1)
        board.step()
        actual = board2framebuffer(board)
        expected = """"""
        assert actual == expected
